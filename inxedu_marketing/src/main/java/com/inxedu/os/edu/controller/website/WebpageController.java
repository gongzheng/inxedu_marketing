package com.inxedu.os.edu.controller.website;

import com.inxedu.os.common.controller.BaseController;
import com.inxedu.os.edu.constants.enums.WebSiteProfileType;
import com.inxedu.os.edu.entity.article.Article;
import com.inxedu.os.edu.service.article.ArticleService;
import com.inxedu.os.edu.service.website.WebsiteProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author www.inxedu.com
 * @description 页面 后台WebpageController
 */
@Controller
public class WebpageController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(WebpageController.class);
	
    @Autowired
    private WebsiteProfileService websiteProfileService;

    @Autowired
    private ArticleService articleService;

	// 绑定属性 封装参数
	@InitBinder("webpage")
	public void initWebpage(WebDataBinder binder) {
		binder.setFieldDefaultPrefix("webpage.");
	}
	

    @RequestMapping("/webpage/toArticle/{articleId}")
    public ModelAndView toArticle(HttpServletRequest request,@PathVariable("articleId") int articleId,ModelAndView model) {
        model.setViewName(getViewPath("/admin/webpage/articleInfo"));
        try {
            // 查询文章详情
            Article article = articleService.queryArticleById(articleId);
            String content = articleService.queryArticleContentByArticleId(articleId);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String time = simpleDateFormat.format(article.getPublishTime());
            model.addObject("date",time);
            model.addObject("article",article);
            model.addObject("content",content);
            Map<String,Object> templatemap=(Map<String,Object>)websiteProfileService.getWebsiteProfileByType(WebSiteProfileType.template.toString()).get(WebSiteProfileType.template.toString());
            model.addObject("templatemap",templatemap);
            //System.out.println(templatemap.get("header"));
            model.addObject("title","资讯详情");
            //修改文章点击数量
            Map<String, String> map = new HashMap<String, String>();
            map.put("num", "+1");
            map.put("type", "clickNum");
            map.put("articleId", articleId + "");
            articleService.updateArticleNum(map);
        }catch (Exception e){
            logger.error("AdminWebpageController.toArticle()--error", e);
            model.setViewName(setExceptionRequest(request, e));
        }
        return model;
    }
}



