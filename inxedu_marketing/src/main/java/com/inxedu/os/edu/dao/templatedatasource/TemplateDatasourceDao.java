package com.inxedu.os.edu.dao.templatedatasource;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.templatedatasource.TemplateDatasource;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板动态数据来源 TemplateDatasourceDao接口
 */
public interface TemplateDatasourceDao{
	/**
     * 添加模板动态数据来源
     */
    public void addTemplateDatasource(TemplateDatasource templateDatasource);
    
    /**
     * 删除模板动态数据来源
     * @param dataKey
     */
    public void delTemplateDatasourceByDataKey(Long dataKey);
    
    /**
     * 修改模板动态数据来源
     * @param templateDatasource
     */
    public void updateTemplateDatasource(TemplateDatasource templateDatasource);
    
    /**
     * 通过dataKey，查询模板动态数据来源
     * @param dataKey
     * @return
     */
    public TemplateDatasource getTemplateDatasourceByDataKey(Long dataKey);
    
    /**
     * 分页查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @param page 分页条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceListPage(TemplateDatasource templateDatasource, PageEntity page);
    
    /**
     * 条件查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceList(TemplateDatasource templateDatasource);
}



