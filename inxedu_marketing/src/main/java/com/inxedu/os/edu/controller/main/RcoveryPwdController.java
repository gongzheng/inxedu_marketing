package com.inxedu.os.edu.controller.main;

import com.inxedu.os.common.cache.CacheUtil;
import com.inxedu.os.common.controller.BaseController;
import com.inxedu.os.common.util.MD5;
import com.inxedu.os.common.util.ObjectUtils;
import com.inxedu.os.common.util.WebUtils;
import com.inxedu.os.edu.entity.system.SysUser;
import com.inxedu.os.edu.service.email.EmailService;
import com.inxedu.os.edu.service.system.SysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/10/27.
 */
@Controller
@RequestMapping("/admin")
public class RcoveryPwdController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private EmailService emailService;
    @Autowired
    private SysUserService sysUserService;
    /**
     * 发送找回密码验证码
     */
    @RequestMapping("/recoveryPwd/sendCode")
    @ResponseBody
    public Map<String,Object> sendCode(HttpServletRequest request){
        Map<String, Object> json = new HashMap<String, Object>(4);
        try {
			/*根据登陆名查找用户信息*/
            String loginName = request.getParameter("userName");
            SysUser sysUser = new SysUser();
            sysUser.setLoginName(loginName);
            SysUser revoceryPwdUser = sysUserService.queryUserByLoginName(sysUser);
			/*如果查到用户为null用户不存在，程序返回*/
            if (ObjectUtils.isNull(revoceryPwdUser)){
                json = this.setJson(false,"该用户不存在!",null);
                return json;
            }
            String code = WebUtils.getRandomNum(4);
            CacheUtil.set("emailCodeNum",code,300);
            emailService.sendMail(revoceryPwdUser.getEmail(),"您的验证码是:"+code+",有效时间为5分钟。","密码找回");
            json = this.setJson(true,"验证码以邮件形式发送至您的邮箱，请注意查收",null);
        }catch (Exception e) {
            logger.error("LoginController.sendCode()---error", e);
            json=this.setAjaxException(json);
        }
        return json;
    }
    /**
     * 跳转找回密码页面
     */
    @RequestMapping("/recoveryPwd/recoverNewPwd")
    @ResponseBody
    public Map<String,Object> recoverNewPwd(HttpServletRequest request){
        Map<String,Object> json = new HashMap<String,Object>();
        try {
            String userLoginName = request.getParameter("userName");
            String code = request.getParameter("code");
            String newPwd = request.getParameter("newPwd");
            String confirmPwd = request.getParameter("confirmPwd");
			/*根据登陆名查找用户信息*/
            SysUser sysUser = new SysUser();
            sysUser.setLoginName(userLoginName);
            SysUser revoceryPwdUser = sysUserService.queryUserByLoginName(sysUser);
			/*如果查到用户为null用户不存在，程序返回*/
            if (ObjectUtils.isNull(revoceryPwdUser)){
                json = this.setJson(false,"该用户不存在!",null);
                return json;
            }
            if (ObjectUtils.isNull(CacheUtil.get("emailCodeNum"))){
                json = this.setJson(false,"请发送验证码！",null);
                return json;
            }
            if (!code.equals(CacheUtil.get("emailCodeNum").toString())){
                json = this.setJson(false,"请输入正确的验证码！",null);
                return json;
            }
            //判断密码格式
            if(!WebUtils.isPasswordAvailable(newPwd)){
                json = this.setJson(false, "密码只能是数字字母组合且大于等于6位小于等于16位", null);
                return json;
            }
            if (!newPwd.equals(confirmPwd)){
                json = this.setJson(false,"新密码和确认密码不一致！",null);
                return json;

            }
            revoceryPwdUser.setLoginPwd(MD5.getMD5(newPwd));
            sysUserService.updateUserPwd(revoceryPwdUser);
            json = this.setJson(true,"修改成功",null);
        }catch (Exception e) {
            logger.error("recoverNewPwd()--error",e);
        }
        return json;
    }
}
