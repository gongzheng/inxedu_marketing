package com.inxedu.os.edu.service.impl.templateoriginal;

import java.util.List;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.templateoriginal.TemplateOriginal;
import com.inxedu.os.edu.service.templateoriginal.TemplateOriginalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.inxedu.os.edu.dao.templateoriginal.TemplateOriginalDao;

/**
 * @author www.inxedu.com
 * @description 模板原始文件 TemplateOriginalService接口实现
 */
@Service("templateOriginalService")
public class TemplateOriginalServiceImpl implements TemplateOriginalService {
	@Autowired
	private TemplateOriginalDao templateOriginalDao;
	
	/**
     * 添加模板原始文件
     */
    public Long addTemplateOriginal(TemplateOriginal templateOriginal){
		return templateOriginalDao.addTemplateOriginal(templateOriginal);
    }
    
    /**
     * 删除模板原始文件
     * @param id
     */
    public void delTemplateOriginalById(Long id){
    	templateOriginalDao.delTemplateOriginalById(id);
    }
    
    /**
     * 修改模板原始文件
     * @param templateOriginal
     */
    public void updateTemplateOriginal(TemplateOriginal templateOriginal){
    	templateOriginalDao.updateTemplateOriginal(templateOriginal);
    }
    
    /**
     * 通过id，查询模板原始文件
     * @param id
     * @return
     */
    public TemplateOriginal getTemplateOriginalById(Long id){
    	return templateOriginalDao.getTemplateOriginalById(id);
    }
    
    /**
     * 分页查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @param page 分页条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalListPage(TemplateOriginal templateOriginal,PageEntity page){
    	return templateOriginalDao.queryTemplateOriginalListPage(templateOriginal, page);
    }
    
    /**
     * 条件查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalList(TemplateOriginal templateOriginal){
    	return templateOriginalDao.queryTemplateOriginalList(templateOriginal);
    }
}



