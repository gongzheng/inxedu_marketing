package com.inxedu.os.edu.controller.webpagetemplate;

import com.inxedu.os.common.controller.BaseController;
import com.inxedu.os.common.util.FileUtils;
import com.inxedu.os.common.util.FreeMarkerUtil;
import com.inxedu.os.common.util.ObjectUtils;
import com.inxedu.os.common.util.StringUtils;
import com.inxedu.os.edu.controller.Template.AdminTemplateFileController;
import com.inxedu.os.edu.entity.webpage.Webpage;
import com.inxedu.os.edu.entity.webpagetemplate.WebpageTemplate;
import com.inxedu.os.edu.entity.website.WebsiteImages;
import com.inxedu.os.edu.entity.website.WebsiteImagesForm;
import com.inxedu.os.edu.service.webpage.WebpageService;
import com.inxedu.os.edu.service.webpagetemplate.WebpageTemplateService;
import com.inxedu.os.edu.service.website.WebsiteImagesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author www.inxedu.com
 * @description 页面模板 后台WebpageController
 */
@Controller
@RequestMapping("/admin")
public class AdminWebpageTemplateController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(AdminWebpageTemplateController.class);
	
	@Autowired
	private WebpageService webpageService;
    @Autowired
    private WebpageTemplateService webpageTemplateService;
    @Autowired
    private WebsiteImagesService websiteImagesService;
    // 绑定属性 封装参数
    @InitBinder("webpageTemplate")
    public void initWebpage(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("webpageTemplate.");
    }

    /**
     * 获取模板数据list
     */
    @RequestMapping("/webpageTemplate/dataList/{id}")
    public ModelAndView getWebpageTemplateDataList(HttpServletRequest request,ModelAndView model, @PathVariable("id") Long id) {
        //model.setViewName(getViewPath("/admin/webpagetemplate/webpage_template_data_list"));
        model.setViewName(getViewPath("/admin/webpagetemplate/updTemplate"));
        try {
            WebpageTemplate webpageTemplate=webpageTemplateService.getWebpageTemplateById(id);
            model.addObject("webpageTemplate", webpageTemplate);

            /*Webpage webPage = webpageService.getWebpageById(webpageTemplate.getPageId());
            model.addObject("webPage", webPage);*/

            //是否发布页面
            //webpageService.updWebpagePublish(webPage.getId());

            //查询模块内容数据
            WebsiteImages websiteImages=new WebsiteImages();
            websiteImages.setWebpageTemplateId(id);
            websiteImages.setQueryOrder("queryAsc");
            List<WebsiteImages> websiteImagesList=websiteImagesService.queryImageList(websiteImages);
            String pageUrl = webpageService.getWebpageById(webpageTemplate.getPageId()).getPublishUrl();
            model.addObject("websiteImagesList", websiteImagesList);
            model.addObject("templateId",id);
            model.addObject("pageUrl",pageUrl);

            //根据模板路径判断 模板数据 显示方式
            model=getWebpageTemplateDataShowStyle(model,webpageTemplate);
            /*webpage资讯页面*/
            Webpage webpageArticle = new Webpage();
            webpageArticle.setPublishUrl("/article_list.html");
            webpageArticle = webpageService.queryWebpageList(webpageArticle).get(0);
			/*webpageTemplate资讯页面下的资讯列表*/
            WebpageTemplate webpageTemplateArticle = new WebpageTemplate();
            webpageTemplateArticle.setPageId(webpageArticle.getId());
            webpageTemplateArticle.setTemplateUrl("/template/templet1/资讯_资讯列表.html");
            webpageTemplateArticle = webpageTemplateService.queryWebpageTemplateList(webpageTemplateArticle).get(0);
            // 资讯模板id
            model.addObject("toArticleListId",webpageTemplateArticle.getId());
        } catch (Exception e) {
            model.setViewName(setExceptionRequest(request, e));
            logger.error("AdminWebpageController.getWebpageTemplateDataList()--error", e);
        }
        return model;
    }

    /**
     * 根据模板路径判断 模板数据 显示方式
     * @param model
     * @param webpageTemplate
     * @return
     */
    public ModelAndView getWebpageTemplateDataShowStyle(ModelAndView model,WebpageTemplate webpageTemplate){
        //上传图片描述
        String upload_image_info="请上传 500*500(长X宽)像素 的图片";
        //是否上传图片
        boolean upload_image=true;
        //是否新增图片
        boolean add_new_image=true;
        //是否删除
        boolean delete_data=true;
        //是否显示标题
        boolean show_title=true;
        //是否显示详情
        boolean show_info=true;
        //是否显示图片广告图点击链接
        boolean show_image_link=false;
        //是否调用接口的动态数据
        boolean dynamic_data=false;
        //是否显示备用标题
        boolean show_titleStandby=false;
        /*是否显示教师简介*/
        boolean show_intruduction = false;
       /* 是否显示首页关于我们内容*/
       boolean show_aboutContent = false;
        /*判断是否是banner图*/
        boolean is_banner = false;
        boolean show_moreFiveTempInfo = false;

        /*显示几行 数据描述 辅助备注， 大于零显示 小于零 不显示， 最后拼接保存到数据info*/
        int show_multi_data_remark=0;
        String templateUrl=webpageTemplate.getTemplateUrl();
        if("/template/templet1/首页_banner.html".equals(templateUrl)){
            upload_image_info="请上传 1920*600(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_关于我们.html".equals(templateUrl)){
            upload_image_info="请上传 538*363(长X宽)像素 的图片";
            upload_image=true;
            show_aboutContent = true;
            add_new_image=true;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_image_link=true;
        }
        else if("/template/templet1/首页_师资力量.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_intruduction=true;
            show_image_link=true;
        }
        else if("/template/templet1/首页_新闻资讯.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
        }

        else if("/template/templet1/首页_banner模板2.html".equals(templateUrl)){
            upload_image_info="请上传 1920*600(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_考试系统模板2.html".equals(templateUrl)){
            upload_image_info="请上传 88*88(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
        }
        else if("/template/templet1/首页_在线学习模板2.html".equals(templateUrl)){
            upload_image_info="请上传 88*88(长X宽)像素 背景透明的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
        }
        else if("/template/templet1/首页_师资力量模板2.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_intruduction=true;
            show_image_link=true;
        }



        else if("/template/templet1/首页_banner模板3.html".equals(templateUrl)){
            upload_image_info="请上传 1920*600(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_课程介绍模板3.html".equals(templateUrl)){
            dynamic_data=true;

            upload_image_info="请上传 1920*600(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
        }
        else if("/template/templet1/首页_师资团队模板3.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_intruduction=true;
            show_image_link=true;
        }


        else if("/template/templet1/首页_banner模板4.html".equals(templateUrl)){
            upload_image_info="请上传 1920*720(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_课程介绍模板4.html".equals(templateUrl)){
            dynamic_data=true;
        }
        else if("/template/templet1/首页_师资团队模板4.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=true;
            show_info=true;
            show_image_link=true;
        }
        else if("/template/templet1/首页_新闻资讯模板4.html".equals(templateUrl)){
            dynamic_data=false;

            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_multi_data_remark = 3;
            /*show_moreTwoInfo = true;*/
            show_title=true;
            show_info=false;
            show_titleStandby =true;

            show_image_link=true;
        }
        else if("/template/templet1/首页_热门话题模板4.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=true;
            show_info=true;
        }



        else if("/template/templet1/课程_广告图.html".equals(templateUrl)){
            upload_image_info="请上传 1920*400(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner =true;
        }
        else if("/template/templet1/课程_优势介绍.html".equals(templateUrl)){
            upload_image_info="请上传 200*200(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=false;
        }
        else if("/template/templet1/课程_精品课程.html".equals(templateUrl)){
            dynamic_data=true;

            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
        }



        else if("/template/templet1/资讯_资讯列表.html".equals(templateUrl)){
            dynamic_data=true;
            model.addObject("redirectIframeUrl","/admin/article/showlist");

            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            show_titleStandby =true;
        }



        else if("/template/templet1/师资_广告.html".equals(templateUrl)){
            upload_image_info="请上传 1920*400(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            is_banner=true;
        }
        else if("/template/templet1/师资_优势介绍.html".equals(templateUrl)){
            upload_image_info="请上传 200*200(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=false;
        }
        else if("/template/templet1/师资_讲师团队.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=true;
            show_info=true;
            show_intruduction =true;
            show_image_link=true;
        }
        else if("/template/templet1/师资_学员心声.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=true;
            show_info=true;
        }



        else if("/template/templet1/关于我们_广告图一.html".equals(templateUrl)){
            upload_image_info="请上传 1920*400(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            show_image_link=true;
        }
        else if("/template/templet1/关于我们_我们是谁.html".equals(templateUrl)){
            upload_image_info="请上传 1920*400(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            is_banner =false;
        }
        else if("/template/templet1/关于我们_我们的优势.html".equals(templateUrl)){
            upload_image_info="请上传 52*52(长X宽)像素 背景透明的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
        }
        else if("/template/templet1/关于我们_我们的服务.html".equals(templateUrl)){
            upload_image_info="请上传 64*64(长X宽)像素 背景透明的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
        }
        else if("/template/templet1/关于我们_找到我们.html".equals(templateUrl)){
            upload_image_info="请上传 64*64(长X宽)像素 背景透明的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=false;
            show_multi_data_remark =4;
        }
        /*模板五*/
        else if("/template/templet1/首页_banner模板5.html".equals(templateUrl)){
            upload_image_info="请上传 1920*590(长X宽)像素 的图片";
            show_image_link=true;
            show_title=false;
            show_info=false;
        }
        else if("/template/templet1/首页_课程分类模板5.html".equals(templateUrl)){
            upload_image_info="请上传 205*185(长X宽)像素 的图片";
            show_image_link=true;
            show_title=false;
            show_info=false;
        }
        else if ("/template/templet1/首页_优秀学员模板5.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            show_multi_data_remark = 3;
            show_info = false;
            /*show_moreTwoInfo =true;*/
            show_image_link=true;
        }
        else if ("/template/templet1/首页_关于我们模板5.html".equals(templateUrl)){
            upload_image_info="请上传 380*250(长X宽)像素 的图片";
            show_image_link=true;
        }
        else if ("/template/templet1/首页_留学资讯模板5.html".equals(templateUrl)){
            dynamic_data = true;
            add_new_image = false;
        }
        //模板六
        else if("/template/templet1/首页_banner模板6.html".equals(templateUrl)){
            upload_image_info="请上传 1920*450(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_迎战未来的4大因素6.html".equals(templateUrl)){
            upload_image_info="请上传 95*95(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_image_link=false;
            is_banner=false;
        }
        else if("/template/templet1/首页_服务需求模板6.html".equals(templateUrl)){
            upload_image_info="请上传 640*357(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=true;
            show_title=true;
            show_multi_data_remark = 2;
            /*show_moreOneInfo = true;*/
            show_info=false;
            show_image_link=true;
            is_banner=false;
        }
        else if("/template/templet1/首页_职业资讯模板6.html".equals(templateUrl)){
            dynamic_data=true;
            upload_image_info="请上传 1920*450(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=true;
            is_banner=false;
        }
        else if("/template/templet1/首页_生活环境模板6.html".equals(templateUrl)){
            upload_image_info="请上传 515*380(长X宽)与380*250（后两张）像素的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_moreFiveTempInfo = true;
            show_image_link=true;
            is_banner=false;
        }

        /*模板七*/
        else if("/template/templet1/首页_banner模板7.html".equals(templateUrl)){
            upload_image_info="请上传 1920*500(长X宽)像素的图片";
            upload_image=true;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_moreFiveTempInfo = false;
            show_image_link=true;
            is_banner=true;
        }
        else if("/template/templet1/首页_特色班级模板7.html".equals(templateUrl)){
            upload_image_info="请上传 640*357(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=false;
            show_image_link=true;
            is_banner=false;
            show_multi_data_remark=3;
        }
        else if("/template/templet1/首页_优秀学员模板7.html".equals(templateUrl)){
            upload_image_info="";
            upload_image=false;
            add_new_image=true;
            delete_data=true;
            show_title=false;
            show_info=false;
            show_image_link=false;
            is_banner=false;
            show_multi_data_remark=6;
        }
        else if("/template/templet1/首页_师资团队模板7.html".equals(templateUrl)){
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=true;
            add_new_image=false;
            delete_data=false;
            show_title=true;
            show_info=true;
            show_image_link=true;
            is_banner=false;
        }
        else if("/template/templet1/首页_新闻资讯模板7.html".equals(templateUrl)){
            dynamic_data=true;
            upload_image_info="请上传 320*320(长X宽)像素 的图片";
            upload_image=false;
            add_new_image=false;
            delete_data=false;
            show_title=false;
            show_info=false;
            show_image_link=false;
            is_banner=false;
        }

        model.addObject("upload_image_info", upload_image_info);
        model.addObject("upload_image", upload_image);
        model.addObject("add_new_image", add_new_image);
        model.addObject("delete_data", delete_data);
        model.addObject("show_title", show_title);
        model.addObject("show_info", show_info);
        model.addObject("dynamic_data", dynamic_data);
        model.addObject("show_titleStandby",show_titleStandby);
        model.addObject("show_intruduction",show_intruduction);
        model.addObject("show_aboutContent",show_aboutContent);
        model.addObject("show_image_link",show_image_link);
        model.addObject("is_banner",is_banner);
        model.addObject("show_moreFiveTempInfo",show_moreFiveTempInfo);
        model.addObject("show_multi_data_remark",show_multi_data_remark);
        return model;
    }
    /**
     * 根据id修改
     */
    @RequestMapping("/webpageTemplate/toUpdate/{id}")
    public ModelAndView toUpdateWebpageTemplate(HttpServletRequest request,ModelAndView model, @PathVariable("id") Long id) {
    	model.setViewName(getViewPath("/admin/webpagetemplate/webpage_template_update"));
    	try {
            WebpageTemplate webpageTemplate=webpageTemplateService.getWebpageTemplateById(id);
            if(webpageTemplate.getTemplateUrl().equals("/template/templet1/首页_关于我们.html")){

            }

            WebsiteImages websiteImages=new WebsiteImages();
            websiteImages.setWebpageTemplateId(id);
            websiteImages.setQueryOrder("queryAsc");
            List<WebsiteImages> websiteImagesList=websiteImagesService.queryImageList(websiteImages);
            model.addObject("websiteImagesList", websiteImagesList);
        } catch (Exception e) {
        	model.setViewName(setExceptionRequest(request, e));
            logger.error("AdminWebpageController.toUpdateWebpageTemplate()--error", e);
        }
        return model;
    }
    /**
     * 更新页面
     */
    @RequestMapping("/webpageTemplate/update")
    public ModelAndView updateWebpageTemplate(HttpServletRequest request,ModelAndView model, @ModelAttribute("webpage") Webpage webpage) {
    	model.setViewName("redirect:/admin/webpage/list");
    	try {
            webpageService.updateWebpage(webpage);

            //根据页面id 删除页面和模板中间表 的记录
            webpageTemplateService.delWebpageTemplateByWebpageId(webpage.getId());

            String templateUrlArr[]=request.getParameterValues("templateUrl");
            String templateNameArr[]=request.getParameterValues("templateName");
            String templateSortArr[]=request.getParameterValues("templateSort");

            WebpageTemplate webpageTemplate=new WebpageTemplate();
            webpageTemplate.setPageId(webpage.getId());
            webpageTemplate.setEffective(1);//是否生效 1生效（默认） 2预览（新增的为2，预览查看所有，发布只查询1的，正式发布后修改为1）
            if(ObjectUtils.isNotNull(templateNameArr)){
                for(int i=0;i<templateUrlArr.length;i++){
                        if(StringUtils.isNotEmpty(templateUrlArr[i])){
                            if (StringUtils.isNotEmpty(templateSortArr[i])){
                            webpageTemplate.setSort(Integer.parseInt(templateSortArr[i]));
                            webpageTemplate.setId(null);
                            webpageTemplate.setTemplateName(templateNameArr[i]);
                            webpageTemplate.setTemplateUrl(templateUrlArr[i]);
                            webpageTemplateService.addWebpageTemplate(webpageTemplate);
                        }
                    }
                }
            }
            if(request.getParameter("isPublish").equals("true")){
                //是否发布页面
                webpageService.updWebpagePublish(webpage.getId(),request.getSession().getServletContext().getRealPath("/"));
            }
        } catch (Exception e) {
            logger.error("AdminWebpageController.updateWebpageTemplate()--error", e);
            model.setViewName(setExceptionRequest(request, e));
        }
        return model;
    }
    /*添加页面对应模板*/
    @RequestMapping("/webpageTemplate/add")
    @ResponseBody
    public Map<String,Object> webpageTemplateAdd(HttpServletRequest request){
        Map<String,Object> json = new HashMap<String,Object>();

        try {
            String templateName = request.getParameter("templateName");
            String templateUrl = request.getParameter("templateUrl");
            long pageId = Long.parseLong(request.getParameter("pageId"));
            int sort = Integer.parseInt(request.getParameter("sort").toString());

            templateUrl = templateUrl.replace(request.getSession().getServletContext().getRealPath("/").replace("\\","/"),"/");
            WebpageTemplate webpageTemplate = new WebpageTemplate();
            webpageTemplate.setPageId(pageId);
            webpageTemplate.setSort(sort);
            webpageTemplate.setTemplateTitle(null);
            webpageTemplate.setInfo(null);
            webpageTemplate.setTemplateUrl(templateUrl);
            webpageTemplate.setTemplateName(templateName);
            webpageTemplate.setEffective(2);//是否生效 1生效（默认） 2预览（新增的为2，预览查看所有，发布只查询1的，正式发布后修改为1）
            Long footerId = Long.parseLong(request.getParameter("footerId"));
            if (StringUtils.isNotEmpty(request.getParameter("footerId"))){
                WebpageTemplate footer = webpageTemplateService.getWebpageTemplateById(footerId);
                footer.setSort(sort+1);
                webpageTemplateService.updateWebpageTemplate(footer);
            }
            webpageTemplateService.addWebpageTemplate(webpageTemplate);
            websiteImagesService.addTemplateData(webpageTemplate.getTemplateUrl(),webpageTemplate.getId());

            //预览发布
            webpageService.updWebpagePublish(pageId,request.getSession().getServletContext().getRealPath("/"));
            json = this.setJson(true,"",webpageTemplate);
        }catch (Exception e){
            json=this.setAjaxException(json);
            logger.error("webpageTemplateAdd()--error",e);
        }
        return json;
    }


    /**
     * 批量修改 WebsiteImages
     */
    @RequestMapping("/WebsiteImages/updWebsiteImagesList")
    @ResponseBody
    public Object updWebsiteImagesList(HttpServletRequest request, WebsiteImagesForm websiteImagesForm,
                                             @ModelAttribute("webpageTemplate") WebpageTemplate webpageTemplate, @RequestParam("webpageTemplateId")Long webpageTemplateId) {
        Map<String,Object> json = new HashMap<String,Object>();
        try {
            //修改模板
            WebpageTemplate updwebpageTemplate =webpageTemplateService.getWebpageTemplateById(webpageTemplateId);
            updwebpageTemplate.setTemplateName(webpageTemplate.getTemplateName());
            updwebpageTemplate.setTemplateTitle(webpageTemplate.getTemplateTitle());
            updwebpageTemplate.setInfo(webpageTemplate.getInfo());
            if (updwebpageTemplate.getEffective()==2){
                updwebpageTemplate.setEffective(1);
            }
            webpageTemplateService.updateWebpageTemplate(updwebpageTemplate);

            //先删除所有的 然后重新保存
            websiteImagesService.delImagesByTemplateId(webpageTemplateId);
            List<WebsiteImages> websiteImagesList=websiteImagesForm.getWebsiteImagesList();
            if(ObjectUtils.isNotNull(websiteImagesList)){
                for(WebsiteImages websiteImages:websiteImagesList){
                    websiteImages.setPublishTime(new Date());
                    websiteImagesService.creasteImage(websiteImages);
                }
            }
            webpageTemplate = webpageTemplateService.getWebpageTemplateById(webpageTemplateId);
            webpageService.updWebpagePublish(webpageTemplate.getPageId(),request.getSession().getServletContext().getRealPath("/"));
            //Webpage webPage = webpageService.getWebpageById(updwebpageTemplate.getPageId());
            //model.setViewName("redirect:/admin/main/index?webPageUrl="+webPage.getPublishUrl());

            //发布页面
            //webpageService.updWebpagePublish(webPage.getId());
            json = this.setJson(true,"","");
        } catch (Exception e) {
            logger.error("AdminWebpageTemplateController.updWebsiteImagesList", e);
            json=this.setAjaxException(json);
        }
        return json;
    }

    /**
     * 根据模板id 获取内容预览
     */
    @RequestMapping("/webpageTemplate/getContentById/{id}")
    public ModelAndView getContentById(HttpServletRequest request,ModelAndView model, @PathVariable("id") Long id) {
        model.setViewName(getViewPath("/admin/webpagetemplate/webpage_template_preview"));
        try {
            WebpageTemplate webpageTemplate = webpageTemplateService.getWebpageTemplateById(id);
            WebsiteImages websiteImages = new WebsiteImages();
            websiteImages.setWebpageTemplateId(id);
            List<WebsiteImages> websiteImagesList = websiteImagesService.queryImageList(websiteImages);
            Map<String, Object> root = new HashMap<>();
            root = webpageService.getTemplateRootMap(webpageService.getWebpageById(webpageTemplate.getPageId()));
            root.put("TEMPLATE_TITLE", webpageTemplate.getTemplateTitle());
            root.put("TEMPLATE_INFO", webpageTemplate.getInfo());
            root.put("WEBSITE_IMAGES_LIST", websiteImagesList);

           /* 创建预览ftl文件*/
            StringBuffer yulanTemplate=new StringBuffer();
            yulanTemplate.append(FileUtils.readLineFile(request.getSession().getServletContext().getRealPath("/")+webpageTemplate.getTemplateUrl()));
            FileUtils.writeFile(request.getSession().getServletContext().getRealPath("/")+"yulan.ftl",yulanTemplate.toString());
            FreeMarkerUtil.analysisTemplate(request.getSession().getServletContext().getRealPath("/"),"yulan.ftl",request.getSession().getServletContext().getRealPath("/")+"yulan.html",root);
            String yulan = AdminTemplateFileController.txt2String(new File(request.getSession().getServletContext().getRealPath("/")+"yulan.html"));
            model.addObject("yulan",yulan);

        } catch (Exception e) {
            model.setViewName(setExceptionRequest(request, e));
            logger.error("AdminWebpageController.getContentById()--error", e);
        }
        return model;
    }
}



