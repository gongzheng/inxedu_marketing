<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<!-- Meta -->
<meta charset="utf-8" http-equiv="Content-Type" />
<!-- End of Meta -->
<!-- Page title -->
<title>${websitemap.web.company}-${websitemap.web.title}</title>
<!-- End of Page title -->
<meta name="author" content="${websitemap.web.author}" />
<meta name="keywords" content="${websitemap.web.keywords}" />
<meta name="description" content="${websitemap.web.description}" />
<link rel="shortcut icon" href="${ctx}/favicon.ico" type="image/x-icon">
<script type="text/javascript" src="${ctx}/static/admin/js/easyTooltip.js"></script>
<script type="text/javascript" src="${ctx}/static/admin/js/jquery-ui-1.7.2.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx}/static/common/jquery.bigcolorpicker.css">
<script type="text/javascript" src="${ctx}/static/admin/js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/tem.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/jquery.bigcolorpicker.js"></script>
<script type="text/javascript" src="${ctx}/static/admin/js/hoverIntent.js"></script>
<script type="text/javascript" src="${ctx}/static/admin/js/superfish.js"></script>
<script type="text/javascript" src="${ctx}/static/admin/js/custom.js"></script>
	<%

		response.setHeader("Cache-Control","no-store");

		response.setHeader("Pragrma","no-cache");

		response.setDateHeader("Expires",0);

	%>
<!-- End of Libraries -->
	<script type="text/javascript">
		/*sMenu*/
		$(function() {
			navFun();
			$(window).resize(function() {navFun();});

			var color = '${color}';

			$("#bigBgshowDiv").css("background",color);
			$("#bigHexColorText").val(color);
			//可视窗口高度 - 头部菜单高度- 底部高度- 内边距
			var windowHeight=$(window).height()-$("#header").height()-$("#footer").height()-80-5;
			$("#mainFrame").attr("height", windowHeight+70);
			//左侧高度
			$("#ui-sMenu").css("height", windowHeight);
		});
		function slideScroll() {
			var prev = $(".prev"),
					next = $(".next"),
					oUl = $(".navList>ul"),
					w = oUl.find("li").outerWidth(true),
					l = oUl.find("li").length;
			oUl.css("width" , w * l + "px");

			//click left
			prev.click(function() {
				if(!oUl.is(":animated")) {
					oUl.animate({"margin-left" : -w}, function() {
						oUl.find("li").eq(0).appendTo(oUl);
						oUl.css("margin-left" , 0);
					});
				}
			});
			//click right
			next.click(function() {
				if(!oUl.is(":animated")) {
					oUl.find("li:last").prependTo(oUl);
					oUl.css("margin-left" , -w);
					oUl.animate({"margin-left" : 0});
				}
			});
		}

		function navFun() {
			var winW = parseInt(document.documentElement.clientWidth, 10) + parseInt(document.documentElement.scrollLeft || document.body.scrollLeft, 10),
					nlW = winW - 620,
					ulW = $(".navList>ul").width(),
					oPN = $('<a href="javascript: void(0)" title="左" class="prev">&nbsp;</a><a href="javascript: void(0)" title="右" class="next">&nbsp;</a>');
			$(".navList").css("width" , nlW);
			if (nlW > ulW) {
				$(".navList").css("width" , nlW);
				$(".prev,.next").remove();
			} else {
				$(".navList").css("width" , nlW);
				oPN.appendTo(".nav-wrap");
				slideScroll();
			}
		}

		//首页的定时判断iframe高度
		function reinitIframe(id){
			var iframe = document.getElementById(id);
			try{
				var bHeight = iframe.contentWindow.document.body.scrollHeight;
				var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
				var height = Math.max(bHeight, dHeight);
				iframe.height = height;
			}catch (ex){}
		}
		//首页的定时判断iframe高度结束定时
		function reinitIframeEND(obj){
			var iframe = obj;
			try{
				var bHeight = iframe.contentWindow.document.body.scrollHeight;
				var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
				var height = Math.max(bHeight, dHeight);
				iframe.height = height;
			}catch (ex){}
			// 停止定时
			window.clearInterval(iframetimer);

		}
		/**
		 * 发布
		 */
		function addTemplate() {
            $.ajax({
                url:baselocation+'/admin/webpage/add',
                type:'post',
                data:$("#addTemplate").serialize(),
                async:false,
                dataType:'json',
                success:function(result){
					if(result.success){
						msgshow("保存成功");
						$("#mainFrame").attr("src", baselocation+"/admin/main?webPageUrl="+result.message);
						//模板 是否 修改 過(排序 ，刪除 ，新增)
						isDataChange=false;//发布后false
						getWebpageTemplates(result.message);
					}
					else{
						msgshow("保存失败",'false');
					}

                    //$("#mainFrame").find("#indexFrame").reload()
                }
            })
		}

		/**
		 * 预览页面
		 */
		function previewPageTemplate() {
			$.ajax({
				url:baselocation+'/admin/ajax/webpage/preview',
				type:'post',
				data:$("#addTemplate").serialize(),
				async:false,
				dataType:'json',
				success:function(result){
					if(result.success){
						$('iframe').contents().find('iframe').css("visibility","hidden");
						//先跳转到main
						$("#mainFrame").attr("src", baselocation+"/admin/main/index?webPageUrl="+result.entity.publishUrl);
						window.setTimeout("$('iframe').contents().find('iframe').attr('src','"+result.message+"');$('iframe').contents().find('iframe').css('visibility','visible');",2000);
					}
				}
			})
		}
		/**
		 * 预览页面
		 */
		function openPageTemplate() {
			$.ajax({
				url:baselocation+'/admin/ajax/webpage/preview',
				type:'post',
				data:$("#addTemplate").serialize(),
				async:false,
				dataType:'json',
				success:function(result){
					if(result.success){
						$('iframe').contents().find('iframe').css("visibility","hidden");
						//先跳转到main
						window.open(baselocation+result.message)
					}
				}
			})
		}
		/**
		 * 新增模板
		 * @param templateName
		 * @param templateUrl
         * @param webpageId
         */
		function appendTemplate(templateName,templateUrl,webpageId){
			var footerId="";
			if ($(".hiddenSort").length<0){
				var sortArr = 1;
			}else {
				var sortArr = $(".hiddenSort").last().val();
				sort = parseInt(sortArr)+1;
				$(".hiddenSort").last().val(sort);
				footerId = $(".hiddenSort").last().next().val()
			}
			$.ajax({
				url:baselocation+'/admin/webpageTemplate/add',
				type:'post',
				data:{"templateName":templateName,"templateUrl":templateUrl,"pageId":webpageId,"sort":sortArr,"footerId":footerId},
				async:false,
				dataType:'json',
				success:function(result){
					var html ='<dl class="acd-sub-dl"><dt>'+
							'<a href="javascript:void(0)"onclick="updSingleTemplate('+result.entity.id+',\''+result.entity.templateName+'\')"target="mainFrame"  title="'+result.entity.templateName+'">'+result.entity.templateName+'</a>'+
							'<a href="javascript:void(0)" onclick="moveUp(this)" title="向上一级" class="acd-ico-btn a-i-b-up"></a>'+
							'<input value="'+sortArr+'"name="templateSort" class="hiddenSort" type="hidden">'+
							'<input type="hidden" value="'+result.entity.id+'" name="templateId"  class="hiddenDataId">'+
							'<input type="hidden" value="'+templateUrl+'"  name="templateUrl">'+
							'<input type="hidden" value="'+result.entity.templateName+'"  name="templateName">'+
							'<input type="hidden" value=""  name="templateTitle">'+
							'<input type="hidden" value=""  name="info">'+
							'<a href="javascript:void(0)"onclick="moveDown(this)" title="向下一级" class="acd-ico-btn a-i-b-dwon"></a>'+
							'<a href="javascript:void(0)" onclick="$(this).parent().parent().remove();$(\'iframe\').contents().find(\'iframe\').contents().find(\'#TEMPLATE_LABEL_'+result.entity.id+'\').remove();isDataChange=true;" title="删除" class="acd-ico-btn a-i-b-close"></a>'+
							'</dt></dl>';
					$("#addTemplate").children().last().before(html);
					//预览页面
					//previewPageTemplate();
					//到模板内容修改页面
					/*window.open("/admin/webpageTemplate/dataList/"+result.entity.id,"mainFrame");*/
					updSingleTemplate(result.entity.id,""+result.entity.templateName+"");
					//模板 是否 修改 過(排序 ，刪除 ，新增)
					isDataChange=true;
				}

			})


		}

		$(window).bind('unload',function(){
			if(isDataChange==true){
				return'刚才的操作，还未发布，确定要离开吗？';
			}
		});
		$(window).bind('beforeunload',function(){
			if(isDataChange==true){
				return'刚才的操作，还未发布，确定要离开吗？';
			}
		});
		/*使用模板 设为首页*/
		function setHelloPage(webpageId) {
			$.ajax({
				url:'/admin/ajax/setHelloPage/'+webpageId,
				type:'post',
				dataType:'json',
                beforeSend:function(){
                    var i = 5;
                    msgshow('正在更换页面请稍等！'+i);
                    var count = setInterval(function () {
                        if (i>1){
                            msgshow('正在更换页面请稍等！'+parseInt(i-1));
                            i--
                        }else{
                            i=5;
                            clearInterval(count);
                            //重新加载
                            window.location.reload();

                            //重新加载页面 和 左侧菜单
                            //getWebpageTemplates('/index.html',null);
                        }
                    },900);

                    //模板 是否 修改 過(排序 ，刪除 ，新增)
                    isDataChange=false;//還原后默認false

                },
				success:function(result){
					if(result.success==true){
						/*var i = 3;
							var count = setInterval(function () {
								if (i>0){
									msgshow(result.message+i);
									i--
								}else{
									i=3;
									clearInterval(count);
									//重新加载
									window.location.reload();

									//重新加载页面 和 左侧菜单
									//getWebpageTemplates('/index.html',null);
								}
							},600);

						//模板 是否 修改 過(排序 ，刪除 ，新增)
						isDataChange=false;//還原后默認false*/

					}else{
						msgshow(result.message);
					}
				},
				error:function(error){
					msgshow("系统繁忙，请稍后再操作！");
				}
			});
		}
		/**
		 * 还原
		 */
		function returnWebpage(id,em){
			if(!confirm('确实要还原吗?')){
				return;
			}
			$.ajax({
				url:'/admin/ajax/webpageReturn/'+id,
				type:'post',
				dataType:'json',
				success:function(result){
					if(result.success==true){
						msgshow("还原成功");

						//模板 是否 修改 過(排序 ，刪除 ，新增)
						isDataChange=false;//還原后默認false
						//重新加载 左側菜單
						getWebpageTemplates(result.entity.publishUrl);
						//重新加载
						$("#mainFrame").reload()

					}else{
						msgshow(result.message);
					}
				},
				error:function(error){
					msgshow("系统繁忙，请稍后再操作！");
				}
			});
		}
		/*跳转导航页面*/
		function navigates(val,obj) {
			$("#logo,#ico,#webInfo,#newTemplate,#watch,#save,#updPwd,#addTemplate").hide();
			$("#return,#addNav").show();
			$(obj).parent().parent().addClass("current");
			$("#mainFrame").attr("src","/admin/website/navigates?type="+val);
		}
		/*新建导航*/
		function addNavigates() {
			var html = '<tr ><td class="navigateName"><input  type="text" value="新建名称"></td>' +
					'<td class="navigateUrl"><input class="navigateUrl" type="text" value=""></td>' +
					'<td class="navigateNewPage"><select ><option value ="1">否</option><option value ="0">是</option></select></td>' +
					'<td class="navigateOrderNum"><input  type="hidden" value="0"></td>' +
					'<td class="navigateStatus"><input  type="hidden"value="0"><p>正常</p></td>' +
					'<td class="c_666 czBtn"><button type="button" onclick="saveNav(this)" class="ui-state-default ui-corner-all ui-btn save">保存</button>' +
					'<button type="button" class="ui-state-default ui-corner-all ui-btn delete" onclick="$(this).parent().parent().remove()">删除</button>' +
					'<button type="button" class="ui-state-default ui-corner-all ui-btn lock" onclick="lockNav(this)">冻结</button></td></tr>';
			$('iframe').contents().find('#tabS_02').append(html)
		}
		$(function () {
			var webpageId =  '${webpage.id}';
			$("#"+webpageId).addClass("current");
		});
		/*切换到页面管理*/
		function templateController() {
			$("#navController,#addTemplate,#newTemplate").show();
			$("#webPage").addClass("current");
			$("#system").removeClass("current");
			$("#webController").hide();
			getWebpageTemplates('/index.html',this)
		}
		/*切换到网站配置管理*/
		function webController() {
			$("#navController,#addTemplate,#newTemplate").hide();
			$("#save").parent().hide();
			$("#webPage").removeClass("current");
			$("#system").addClass("current");
			$("#return,#addNav").hide();
			$("#webController,#updPwd,#webInfo").show()
		}
		/*更新单个模板内容*/
		function updSingleTemplate(templateId,templateName) {
			$("#webUpd").hide();
			$("#intoTemplateInfo").show();
			$("#templateName").html(templateName);
			$("#mainFrame").attr("src",baselocation+'/admin/webpageTemplate/dataList/'+templateId) ;
		}

		function saveImage() {
			$("#mainFrame")[0].contentWindow.saveImage();
            isDataChange=false;
		}
		/*更新模板信息页面的返回方法*/
		function returnPage() {
			$('#mainFrame')[0].contentWindow.returnPage()
        }
        /*到更新网站配置页面*/
        function toWebsiteProfile(obj) {
        	$(obj).parent().parent().parent().children().removeClass("current");
        	$(obj).parent().parent().addClass("current");
			$("#mainFrame").attr("src",baselocation+"/admin/websiteProfile/find/web")
		}
		/*修改信息跳转页面*/
		function updPwd(obj) {
			if(isDataChange==false || confirm('刚才的操作，还未发布，确定要离开吗？')){
				$(obj).parent().parent().parent().children().removeClass("current");
				$(obj).parent().parent().addClass("current");

				$("#mainFrame").attr("src",baselocation+"/admin/sysuser/user/toupdatepwd")
			}
		}
		/*添加新模板*/
		function addNewTemplate(pageId) {
			$("#save").hide();
			$("#return").show();
			$("#mainFrame").attr("src",baselocation+"/admin/main/addTemplate?webpageId="+pageId);
		}
		/*点击退出登陆处的修改信息跳转页面*/
		function updPwdNotIndex(obj) {
			getWebpageTemplates('/index.html',this);
			webController();
			if(isDataChange==false || confirm('刚才的操作，还未发布，确定要离开吗？')){
				$(obj).parent().parent().parent().children().removeClass("current");
				$(obj).parent().parent().addClass("current");

				$("#mainFrame").attr("src",baselocation+"/admin/sysuser/user/toupdatepwd")
			}
		}

		var redirectIframeUrl="";
		/**
		 * indexFrame 是修改 添加 页面时
		 */
		function updIndexFrame(redirectIframeUrlTemp) {
			$("#normalReturn").hide();
			$("#updIndexFrame").show();
			redirectIframeUrl=redirectIframeUrlTemp;
		}

		/**
		 * updIndexFrame点击跳转执行方法
		 */
		function updIndexFrameJump() {
			$('iframe').contents().find("#indexFrame").attr("src",redirectIframeUrl);
			//显示模板 内容介绍
			document.getElementById("mainFrame").contentWindow.showTmplateInfo();

			//indexFrame 是列表 页面时
			listIndexFrame();
		}
		/**
		 * indexFrame 是列表 页面时
		 */
		function listIndexFrame() {
			$("#normalReturn").show();
			$("#updIndexFrame").hide();
		}
</script>
</head>
<body>
	<!-- Container -->
	<div class="tHeader headerimg">
		<div>
			<!-- Header -->
			<div id="header">

				<!-- Top -->
				<div id="top">
					<!-- Logo -->
					<div class="logo">
							<a href="/" target="_blank" title="因酷在线教育软件 - 在线教育整体解决方案提供商" class="tooltip">
								<img src="${ctx}/static/admin/assets/logo.png" height="56" alt="因酷在线教育软件 - 在线教育整体解决方案提供商" />
							</a>
					</div>
					<!-- End of Logo -->
					<%--<div class=" nav-bar headerhtml" id="menuHeader">
						<jsp:include page="/WEB-INF/view/inxedu/admin/main/header.jsp" />
						<div class="nav-wrap">
							<div class="navList">
								<ul>
								</ul>
							</div>
						</div>
					</div>--%>
					<!-- Meta information -->
					<div class="meta">
					<%--	<p>欢迎来到${websitemap.web.company}后台管理系统!</p>--%>
						<ul>
							<%--<li>

									<em class="icon-xt icon-tc"></em>
									<span class="vam">退出系统</span>
								</a>
							</li>--%>
							<li class="ge-warp">
								<a href="javascript:void(0)" title="${sysuser.userName}" class="gezx tit-name">
									<em class="icon-xt icon-ge"></em>
									<span class="vam">${sysuser.userName}</span>
									<em class="icon-xt icon-down"></em>
								</a>
								<ol class="ge-down-list t-down-box">
									<%--<em class="g-d-l-sj">
										<img src="${ctx}/static/admin/assets/r-d-l-sj.png">
									</em>--%>
								<%--	<li>
										<a href="/admin/sysuser/user/toUpdUserInfo" target="mainFrame" title="个人资料">个人资料</a>
										</li>--%>
									<li>
										<a href="javascript:void(0)" onclick="updPwdNotIndex($('#userInfo'))" target="mainFrame" title="修改信息">修改信息</a>
									</li>
									<li>
										<a href="javascript:void(0)" onclick="if(isDataChange==false || confirm('刚才的操作，还未发布，确定要离开吗？')){window.open('/admin/outlogin','_self');}" title="退出系统" class="">退出系统</a>
									</li>
								</ol>
							</li>

							<li>
								<form action="${ctx}/admin/main/updColor" id="updColor" method="post">
								<a href="javascript:void(0)" title="${sysuser.loginName}" class="tit-name">
									<em class="icon-xt icon-ys"></em>
									<span class="vam">颜色</span>
									<em class="icon-xt icon-down"></em>
								</a>
								<ol class="ys-down-list t-down-box">
									<%--<em class="g-d-l-sj">
										<img src="${ctx}/static/admin/assets/r-d-l-sj.png">
									</em>--%>
									<li>
										<div class="colo-box">
										</div>
									</li>
								</ol>
								</form>
							</li>
							<li>
								<a href="javascript:void(0)" title="${sysuser.loginName}" class="tit-name">
									<em class="icon-xt icon-mb"></em>
									<span class="vam">主题</span>
									<em class="icon-xt icon-down"></em>
								</a>
								<ol class="t-down-box mb-down-box">
									<%--<em class="g-d-l-sj">
										<img src="${ctx}/static/admin/assets/r-d-l-sj.png">
									</em>--%>
									<%--<div class="ts-box">

									</div>--%>
									<li>
										<div class="m-d-warp">
											<div class="mb-wrap">
												<ul class="clearfix mb-list">
													<%--<li id="7">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题一" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-1.jpg">
																</a>
															</div>
															<div class="ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题一</h5>
																&lt;%&ndash;  <p class="txt-nr">
                                                                      焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*600px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                  </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(7)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>
													<li id="47">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题二" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-2.jpg">
																</a>
															</div>
															<div class=" ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题二</h5>
																&lt;%&ndash; <p class="txt-nr">
                                                                     焦点图轮播、各平台功能特点介绍等。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                 </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(47)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>
													<li id="48">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题三" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-3.jpg">
																</a>
															</div>
															<div class="ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题三</h5>
																&lt;%&ndash; <p class="txt-nr">
                                                                     焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*600px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                 </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(48)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>
													<li id="49">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题四" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-4.jpg">
																</a>
															</div>
															<div class="ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题四</h5>
																&lt;%&ndash;<p class="txt-nr">
                                                                    焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(49)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>
													<li id="134">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题五" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-4.jpg">
																</a>
															</div>
															<div class="ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题五</h5>
																&lt;%&ndash;<p class="txt-nr">
                                                                    焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(134)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>
													<li id="135">
														<div class="nr-box">
															<div class="pic">
																<a href="/static/admin/mb-pic/mb-1.jpg" target="_blank" rel="lightbox[plants]" title="主题六" class="lightbox-enabled">
																	<img src="/static/admin/mb-pic/mb-4.jpg">
																</a>
															</div>
															<div class="ml10 mr10">
																<h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">主题六</h5>
																&lt;%&ndash;<p class="txt-nr">
                                                                    焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                                                </p>&ndash;%&gt;
															</div>
														</div>
														<div class="tac mt20">
															<a href="javascript:void(0)" onclick="setHelloPage(135)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
														</div>
													</li>--%>
                                                        <li id="7">
                                                            <a href="javascript:void(0)" onclick="setHelloPage(7)" class="mb-box mb-box-3">

                                                                <em class="xz-bz">
                                                                    <img src="${ctx}/static/admin/assets/nav-fl-xz.png">
                                                                </em>
                                                            </a>
                                                        </li>
													<li id="47">
														<a href="javascript:void(0)" onclick="setHelloPage(47)" class="mb-box mb-box-1">

															<em class="xz-bz">
																<img src="${ctx}/static/admin/assets/nav-fl-xz.png">
															</em>
														</a>
													</li>
													<li id="48">
														<a href="javascript:void(0)" onclick="setHelloPage(48)" class="mb-box mb-box-2">

															<em class="xz-bz">
																<img src="${ctx}/static/admin/assets/nav-fl-xz.png">
															</em>
														</a>
													</li>
                                                        <li id="49">
                                                            <a href="javascript:void(0)" onclick="setHelloPage(49)" class="mb-box mb-box-6">

                                                                <em class="xz-bz">
                                                                    <img src="${ctx}/static/admin/assets/nav-fl-xz.png">
                                                                </em>
                                                            </a>
                                                        </li>
                                                        <li id="1">
                                                            <a href="javascript:void(0)" onclick="setHelloPage(1)" class="mb-box mb-box-5">

                                                                <em class="xz-bz">
                                                                    <img src="${ctx}/static/admin/assets/nav-fl-xz.png">
                                                                </em>
                                                            </a>
                                                        </li>
                                                        <li id="2">
                                                            <a href="javascript:void(0)" onclick="setHelloPage(2)" class="mb-box mb-box-7">

                                                                <em class="xz-bz">
                                                                    <img src="${ctx}/static/admin/assets/nav-fl-xz.png">
                                                                </em>
                                                            </a>
                                                        </li>
													<li id="3">
														<a href="javascript:void(0)" onclick="setHelloPage(3)" class="mb-box mb-box-4">

															<em class="xz-bz">
																<img src="${ctx}/static/admin/assets/nav-fl-xz.png">
															</em>
														</a>
													</li>



													<div class="clear"></div>
												</ul>
												<div class="tar">
													<a href="javascript:void(0)" onclick="setHelloPage(${defaultTemplateId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">恢复默认</a>
												</div>
											</div>
										</div>
									</li>
								</ol>
							</li>
						</ul>
					</div>
					<!-- End of Meta information -->
				</div>
				<!-- End of Top-->
			</div>
		</div>
	</div>
	<!-- End of Header -->
	<div id="container">

		<!-- Background wrapper -->
		<div id="bgwrap">
			<!-- Main Content -->
			<div id="content">
				<div id="main">
					<div id="right">
					<iframe id="mainFrame" name="mainFrame" src="${ctx}/admin/main/index" style="overflow:visible;" scrolling="yes" frameborder="no" width="100%" height=""></iframe>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->
			<!-- Sidebar -->
			<div id="sidebar">
				<!-- <h2>菜单目录 / MENU</h2> -->
				<!-- Accordion -->
				<div id="ui-sMenu" class="ui-accordion ui-widget ui-helper-reset" style="height:760px;overflow-y: auto">

				</div>
			</div>
			<!-- End of Sidebar -->
		</div>
		<!-- End of bgwrap -->
	</div>
	<!-- End of Container -->
	<!-- Footer -->
	<div id="footer">
		<p class="mid">${websitemap.web.copyright}<span class="ml40">Powered By </span><a target="_blank" href="http://www.inxedu.com/" style="color: #666;">${websitemap.web.company}</a></p>
	</div>
	<script type="text/javascript">
		//模板 是否 修改 過(排序 ，刪除 ，新增)
		var isDataChange=false;
		$(function(){
			$("#top div.meta li").hover(function(){
				$(this).find(".tit-name").addClass("hover");
				$(this).find(".t-down-box").stop().slideUp('fast').show();
			},function(){
				$(this).find(".tit-name").removeClass("hover");
				$(this).find(".t-down-box").stop().slideDown('fast').hide();
			});
			//二级菜单选中
			$("#ui-sMenu .ui-accordion-content a").click(function(){
				//二级菜单下所有的移除选中样式
				$("#ui-sMenu .ui-accordion-content a").removeClass("current");
				//二级菜单选中
				$(this).addClass("current").parent().parent().siblings().find("a").removeClass("current");
			});

			getWebpageTemplates("/index.html");
		});



		/**
		 * 根据路径获取 所有的 模板 显示在 左侧编辑器
		 * @param webpageUrl
		 */
		function getWebpageTemplates(webpageUrl,obj) {
			if($(obj).parent().hasClass("current")){
			 	return;
			 }
			if(isDataChange==false || confirm("刚才的操作，还未保存，确定要离开吗？")){
				//模板 是否 修改 過(排序 ，刪除 ，新增)
				isDataChange=false;//重新加载默认 false

				$("#mainFrame").attr("src",webpageUrl+"?random="+Math.random());
				//window.open('/admin/main/index?webPageUrl='+webpageUrl,'mainFrame');
				$("#ui-sMenu").html("");
				$.ajax({
					url:baselocation+'/admin/main/left',
					data:{"webPageUrl":webpageUrl},
					type:'post',
					async:false,
					dataType:'text',
					success:function(result){
						$("#ui-sMenu").html(result);
					}
				});
			}
		}
	</script>
</body>
</html>


