<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>修改个人信息</title>
<link rel="stylesheet" href="${ctx}/static/common/nice-validator/jquery.validator.css"/>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/jquery.validator.js"></script>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/local/zh-CN.js"></script>
<script type="text/javascript">

	function submitform(){
		$.ajax({
			url:baselocation+'/admin/sysuser/user/updatepwd',
			type:'post',
			data:$("#submitform").serialize(),
			async:false,
			dataType:'json',
			success:function(result){
				if(!result.success){
					var entity = result.entity;
					if(entity=="oldPwdisnull"){
						msgshow("请输入旧密码");
						return;
					}
					if(entity=="newPwdisnull"){
						msgshow("请输入新密码");
						return;
					}
					if(entity=="confirmPwdisnull"){
						msgshow("请输入确认密码");
						return;
					}
					if(entity=="newPwderror"){
						msgshow("请输入6到16位的新密码");
						return;
					}
					if(entity=="newPwdNotEqualsconfirmPwd"){
						msgshow("确认密码和密码不相等");
						return;
					}
					if(entity=="oldPwdIsError"){
						msgshow("旧密码不正确");
						return;
					}
					if(entity=="newPwdEquestOldPwd"){
						msgshow("旧密码和新密码相等");
						return;
					}

				}
				msgshow("成功");
			}
		});
	}
</script>
</head>
<body>

<div class="rMain">
	<div class="">
		<p class="hLh30 fsize20 c-333 f-fH mt30">个人资料管理
			<tt class=" ml20 fsize16">
				（<font color="red">*</font>&nbsp;为必填项）
			</tt>
			<span class="field_desc"></span>
		</p>
		<form action="${ctx}/admin" method="post" id="submitform">
			<p>
				<label for="sf"><font color="red">*</font>&nbsp;用户名:</label>
				<input type="password" name="oldPwd" id="oldPwd" class="{required:true} lf" data-rule="required;"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red">*</font>&nbsp;新密码：</label>
				<input type="password" name="newPwd" id="newPwd" class="{required:true,number:true,min:0,max:1000} lf" data-rule="required;"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red">*</font>&nbsp;确认密码：</label>
				<input type="password" name="confirmPwd" id="confirmPwd" class="{required:true} lf" data-rule="required;"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<input type="button" value="提 交" class="button" onclick="submitform()" />
				<%--<input type="button" value="返 回" class="button" onclick="javascript:history.go(-1);" />--%>
			</p>
		</form>
	</div>
	<!-- /tab4 end -->
</div>
</body>
</html>
