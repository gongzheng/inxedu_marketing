<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>文字着色</title>
    <script type="text/javascript" src="${ctx}/static/common/syntaxhighlighter/scripts/shCore.js"></script>
    <script type="text/javascript" src="${ctx}/static/common/syntaxhighlighter/scripts/shBrushJScript.js"></script>
    <link type="text/css" rel="stylesheet" href="${ctx}/static/common/syntaxhighlighter/styles/shCoreDefault.css"/>

</head>
<body style="background: white; font-family: Helvetica">
<input type="button" value="返 回" class="button" onclick="javascript:history.go(-1);" />
<pre class="brush: js;">

	<c:out value="${str}"></c:out>

	</pre>
<script type="text/javascript">SyntaxHighlighter.all();</script>
</body>

</html>
