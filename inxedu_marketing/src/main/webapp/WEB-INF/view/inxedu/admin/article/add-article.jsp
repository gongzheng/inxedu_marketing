<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加文章</title>
<script src="${ctximg}/static/common/jquery-ui-1.10.4/js/jquery-ui-1.10.4.custom.js"></script>
<script src="${ctximg}/static/common/jquery-ui-1.10.4/js/jquery.ui.datepicker-zh-CN.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/jquery-ui-1.10.4/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript" src="${ctx}/static/admin/article/article.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx}/kindeditor/themes/default/default.css" />
<script type="text/javascript" src="${ctx}/kindeditor/kindeditor-all.js"></script>

<link rel="stylesheet" href="${ctx}/static/common/nice-validator/jquery.validator.css"></link>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/jquery.validator.js"></script>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/local/zh-CN.js"></script>

<script type="text/javascript">
	$(function() {
		initKindEditor_addblog('content', 580, 400, 'articleContent', 'true');
		initSimpleImageUpload('imageFile', 'article', callback);
		
		$("#publishTime").datetimepicker({
			regional:"zh-CN",
	        changeMonth: true,
	        dateFormat:"yy-mm-dd",
	        timeFormat: "HH:mm:ss"
	    });
	});

	/**
	 * 调用父窗口 修改父窗口iframe的高度
	 */
	window.onload = function () {
		window.parent.setIframeHeight()
	};
</script>
<script >
	/**
	 * 保存文章
	 */
	function saveArticle(){
		/*var title = $("input[name='article.title']").val();
		 if(title==null || $.trim(title)==''){
		 msgshow("请填写文章标题！");
		 return false;
		 }
		 var articleType = $("select[name='article.type']").val();
		 if(articleType==0){
		 msgshow("请选择文章类型！");
		 return false;
		 }
		 var content = $("#content").val();
		 if(content==null || $.trim(content)==''){
		 msgshow('请填写文章内容！');
		 return false;
		 }*/
		window.parent.parent.saveArticle();
		$("#articleForm").submit();
	}
</script>
</head>
<body>
<fieldset>
	<legend>
		<span>文章管理</span>
		&gt;
		<span>添加</span>
	</legend>
	<div class="" >
		<form action="${ctx}/admin/article/addarticle" method="post" id="articleForm" data-validator-option="{stopOnError:false, timely:false}">
			<input type="hidden" name="article.imageUrl">
			<table style="line-height: 35px;" width="100%">
				<tr>
					<td width="15%" align="center">
						<font color="red">*</font>标题
					</td>
					<td>
						<input name="article.title" data-rule="标题:required;" type="text" style="width: 580px;" />
					</td>
				</tr>
				<tr>
					<td align="center">摘要：</td>
					<td>
						<textarea name="article.summary" style="width: 580px; height: 90px;" data-rule="required;"></textarea>
					</td>
				</tr>
				<tr style="display:none;">
					<td align="center">
						<font color="red">*</font>文章类型：
					</td>
					<td>
						<select name="article.type">
							<option value="2">文章</option>
						</select>
						<span style="display: none;">
						作者：
						<input name="article.author" value="" type="text" style="width: 100px;" />
						来源：
						<input type="text" value="" name="article.source" />
						</span>
					</td>
				</tr>
				<tr>
					<td align="center">点击数：</td>
					<td>
						<input name="article.clickNum" id="clickNum"   type="text" style="width: 140px;" value="0"  onkeyup="this.value=this.value.replace(/\D/g,'')" data-rule="required;"/>
					</td>
				</tr>
				<tr>
					<td align="center">发布时间：</td>
					<td>
						<input name="article.publishTime" id="publishTime"   readonly="readonly"  type="text" style="width: 140px;" data-rule="required;"/>
					</td>
				</tr>
				<tr>
					<td align="center">封面图片：</td>
					<td>
						<img id="showImage" width="180" height="100" src="/static/admin/assets/logo.png">
						<input type="button" value="上传" id="imageFile" />
						<font color="red">(请上传宽高为： 640*357 的图片)</font>
					</td>
				</tr>
				<tr>
					<td align="center">
						<font color="red">*</font>内容：
					</td>
					<td>
						<textarea name="articleContent.content" id="content" data-rule="required;"></textarea>
					</td>
				</tr>
				<tr>
					<td align="center">排序值：</td>
					<td>
						<input name="article.sort" id="sort"   type="text" style="width: 140px;" value="0"  onkeyup="this.value=this.value.replace(/\D/g,'')" data-rule="required;"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input onclick="saveArticle()" class="button" type="button" value="保存">
					</td>
				</tr>
			</table>
		</form>
	</div>
</fieldset>
</body>
</html>