<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta HTTP-EQUIV="pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate">
	<meta HTTP-EQUIV="expires" CONTENT="0">
	<title>${title}</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="title" content="${title}">
	<meta name="author" content="">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/web.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/ie.css">
	<link href="/template/templet1/css/mw_320_768.css" rel="stylesheet" type="text/css" media="screen and (min-width: 320px) and (max-width: 1024px)">
	<script>
		var str='<link href="/template/templet1/css/color.css?'+Math.random()+'" rel="stylesheet" type="text/css"/>';
		document.write(str);


	</script>
	<!--[if lt IE 9]><script src="/template/templet1/js/html5.js"></script><![endif]-->
	<script src="/template/templet1/js/jquery-1.7.2.min.js" type="text/javascript"></script>

</head>
<body>
<input type="hidden" id="selectPageBtn"  value="selectPageId">
<div class="mbqh-qt" style="display: none">
	<ul>
		<li id="page7" class="li-1">
			<a href="javascript:void (0)"  onclick="setHelloPage(7)" class="f-fM " title="简约风">经典版</a>
		</li>
		<li id="page47" class="li-2">
			<a href="javascript:void (0)"  onclick="setHelloPage(47)" class="f-fM " title="小清新">简约风</a>
		</li>
		<li id="page48" class="li-3">
			<a href="javascript:void (0)"  onclick="setHelloPage(48)" class="f-fM " title="经典版">小清新</a>
		</li>
		<li id="page49" class="li-4">
			<a href="javascript:void (0)"  onclick="setHelloPage(49)" class="f-fM " title="创意秀">时尚感</a>
		</li>
		<li id="page1" class="li-5">
			<a href="javascript:void (0)"  onclick="setHelloPage(1)" class="f-fM " title="科技风">科技风</a>
		</li>
		<li id="page2" class="li-6">
			<a href="javascript:void (0)"   onclick="setHelloPage(2)" class="f-fM " title="时尚感">致青春</a>
		</li>
		<li id="page3" class="li-7">
			<a href="javascript:void (0)" onclick="setHelloPage(3)" class="f-fM " title="致青春">创意秀</a>
		</li>
	</ul>
</div>
<style>
    #message{width: 260px;height: 48px;position: absolute;left: 50%;z-index: 999;}
    .msg-nr{border-radius: 6px;overflow: hidden;border: 1px solid #d6d6d6;background: #fff;text-align: center;}
    .msg-nr span{font-size: 14px;line-height: 48px;text-align: center;margin: 0;display: inline-block;}
    .msg-nr .rts-ico{display: inline-block;vertical-align: middle;width: 20px;height: 20px;margin-right: 5px;}
    .msg-nr .rts-ico img{display: block;width: 100%;height: 100%;}
</style>
<script>
	$(function(){
		if (self ==top){
            var url=window.location.pathname;

            //if(url=="/"||url=="/index.html"){
                $(".mbqh-qt").show();
            //}
		}
		/*切换页面按钮选中添加状态*/
        var pageId = $("#selectPageBtn").val();
        var id = "#page"+pageId;
        $(id).addClass("current")
	});
    /**
     * 弹出框 （弱提示）
     * @param info
     */
    function msgshow(info,success){
        $("#message").remove();
        $(".mes-warp-0").remove();
        var msgBox = $('<div id="message"></div>').appendTo($("body")).fadeIn("fast").delay(3000).fadeOut("slow");
        /*var msgTxt = [
            '<div class="mes-warp-0">'+
            '<div class="msg-cg msg-nr"><p>恭喜你修改成功！</p></div>'+
            '</div>'
            ,
            '<div class="mes-warp-1">'+
            '<div class="msg-sb msg-nr"><p>修改失败！</p></div>'+
            '</div>'
        ];*/
        if (success=="false"){
            var msgTxt =
                    '<div class="mes-warp-0">'+
                    '<div class="msg-cg msg-nr msg-nr-cw"><em class="rts-ico"><img src="/static/admin/assets/rts-cw.png"></em><span>'+info+'</span></div>'+
                    '</div>'
        }else{
            var msgTxt =
                    '<div class="mes-warp-0">'+
                    '<div class="msg-cg msg-nr msg-nr-zq"><em class="rts-ico"><img src="/static/admin/assets/rts-zq.png"></em><span>'+info+'</span></div>'+
                    '</div>'
        }


        $("#message").html(msgTxt);
        var dTop = (parseInt(document.documentElement.clientHeight, 10)/2) + (parseInt(document.documentElement.scrollTop || document.body.scrollTop, 10)),
                dH = msgBox.height(),
                dW = msgBox.width(),
                timer = null,
                dClose;
        msgBox.css({"top" : (dTop-(dH/2)) , "margin-left" : -(dW/2)});
        dClose = function() {msgBox.remove();};
    }
	/*使用模板 设为首页*/
	function setHelloPage(webpageId) {
		$.ajax({
			url:'/admin/ajax/setHelloPage/'+webpageId,
			type:'post',
			dataType:'json',
            beforeSend:function(){
                var i = 5;
                msgshow('正在更换页面请稍等！'+i);
                var count = setInterval(function () {
                    if (i>1){
                        msgshow('正在更换页面请稍等！'+parseInt(i-1));
                        i--
                    }else{
                        i=5;
                        clearInterval(count);
                        //重新加载
                        window.location.reload();

                        //重新加载页面 和 左侧菜单
                        //getWebpageTemplates('/index.html',null);
                    }
                },900);

                //模板 是否 修改 過(排序 ，刪除 ，新增)
                isDataChange=false;//還原后默認false

            },
			success:function(result){
				if(result.success==true){
					/*var i = 3;
					var count = setInterval(function () {
						if (i>0){
                            msgshow(result.message+i);
							i--
						}else{
							i=3;
							clearInterval(count);
							//重新加载
							window.location.reload();

							//重新加载页面 和 左侧菜单
							//getWebpageTemplates('/index.html',null);
						}
					},600);

					//模板 是否 修改 過(排序 ，刪除 ，新增)
					isDataChange=false;//還原后默認false*/

				}else{
					msgshow(result.message);
				}
			},
			error:function(error){
				msgshow("系统繁忙，请稍后再操作！");
			}
		});
	}
</script>
<div class="in-wrap i-main" >
	<div class="h-mobile-mask"></div>

	<div class="head-mobile">
		<div class="head-mobile-box">
				<nav class="mw-nav">
					<ul class="clearfix headerhtml">
							<li >
								<a name="/index.html" href="javascript:void(0)" title="首页" onclick="getWebpageTemplates('/index.html',this)">首页</a>
							</li>
							<li >
								<a name="/course.html" href="javascript:void(0)" title="课程" onclick="getWebpageTemplates('/course.html',this)">课程</a>
							</li>
							<li >
								<a name="/teacher.html" href="javascript:void(0)" title="名师" onclick="getWebpageTemplates('/teacher.html',this)">名师</a>
							</li>
							<li >
								<a name="/article_list.html" href="javascript:void(0)" title="资讯" onclick="getWebpageTemplates('/article_list.html',this)">资讯</a>
							</li>
							<li >
								<a name="/about_us.html" href="javascript:void(0)" title="关于我们" onclick="getWebpageTemplates('/about_us.html',this)">关于我们</a>
							</li>
					</ul>
				</nav>
		</div>
	</div>
	<section  id="aCoursesList">

		<div id="temOne">
			<section>
				<header id="header" class="pa-header">
					<div class="container">
						<section class="header-wrap">
							<h1 class="logo">
								<a href="/" title="因酷在线教育软件">
									<img src="/template/templet1/images/logo.png" alt="因酷在线教育软件">
								</a>
							</h1>
							<!-- /nav -->
								<div id="nav" class="nav-left nav">
									<ul id="ifCurrent" class="clearfix">
											<li >
												<a name="/index.html" href="javascript:void(0)"class="nav-fir" onclick="getWebpageTemplates('/index.html','1',this)" title="首页" >首页</a>
											</li>
											<li >
												<a name="/course.html" href="javascript:void(0)"class="nav-fir" onclick="getWebpageTemplates('/course.html','1',this)" title="课程" >课程</a>
											</li>
											<li >
												<a name="/teacher.html" href="javascript:void(0)"class="nav-fir" onclick="getWebpageTemplates('/teacher.html','1',this)" title="名师" >名师</a>
											</li>
											<li >
												<a name="/article_list.html" href="javascript:void(0)"class="nav-fir" onclick="getWebpageTemplates('/article_list.html','1',this)" title="资讯" >资讯</a>
											</li>
											<li >
												<a name="/about_us.html" href="javascript:void(0)"class="nav-fir" onclick="getWebpageTemplates('/about_us.html','1',this)" title="关于我们" >关于我们</a>
											</li>
									</ul>
								</div>
							<aside class="mw-nav-btn">
								<div class="mw-nav-icon"></div>
							</aside>
							<!-- /nav-mobile -->
						</section>
					</div>
				</header>
			</section>
		</div>
<div id="temTwo">
    <section id="demo4-main" class="all-main">
        <div class="new-list-warp">
            <section class="container">
                <div class="comm-title">
                    <header class="clearfix">
                        <h3 class="unFw hLh30 pb10 fl">
                            <span class="disIb fsize24 f-fH c-333">资讯详情</span>
                            <span class="disIb fsize24 f-fM c-master ml5 mr5">/</span>
                            <span class="disIb fsize12 f-fM c-999">DETAILS</span>
                        </h3>
                        <span class="fr hLh20 mt10">
							<a class="go-back disFw" style="text-decoration: none" href="javascript:history.go(-1)" title="返回">
								<em class="icon20 ico">
									<img src="http://127.0.0.1:81/template/templet1/images/go-back.png">
								</em>
								<tt class="fsize14 c-999 f-fM">返回</tt>
							</a>
						</span>
                    </header>
                </div>
            </section>
            <div>
                <section class="container">
                    <article class="mt50 pb20 n-a-tit">
                        <h2 class="tac unFw">
                            <font class="fsize24 c-333 f-fH">${article.title}</font>
                        </h2>
                        <div class="hLh30 tac mt10 of fenl">
							<span class="disIb">
								<em class="icon16 ico">
									<img src="http://127.0.0.1:81/template/templet1/images/liul.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam">${article.clickNum}次</tt>
							</span>
                            <span class="disIb mr30 ml30">
								<em class="icon16 ico">
									<img src="http://127.0.0.1:81/template/templet1/images/time.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam"> ${date}</tt>
							</span>
                            <span class="disIb">
								<em class="icon16 ico">
									<img src="http://127.0.0.1:81/template/templet1/images/laiy.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam">${article.author}</tt>
							</span>
                        </div>
                    </article>
                    <article class="n-a-warp mt20">
                        <p>${content}</p>
                    </article>
                </section>
            </div>
        </div>
    </section>
</div>


</section>
<div id="temSix">
	<section>
				<footer class="footer">
			<div class="container">

				<section class="clearfix">
					<div class="col-8 fl">
						<div class="txt">
							<p class="tac">
								<span>
									<a href="/about_us.html" title="关于我们">关于我们</a>
								</span>
								<span>
									服务热线：400-900-8560
								</span>
								<span>
									Email：serivce@inxedu.com
								</span>
							</p>
							<p class="tac">
								<span>
									Copyright ©2013-2016 版权所有北京因酷时代科技有限公司京ICP备15053495号 京公网安备11010802018055
								</span>
							</p>
							<p class="tac">
								<span>
									<a href="http://www.inxedu.com" target="_blank" title="因酷软件">Powered by 因酷教育软件</a>
								</span>
							</p>
						</div>
					</div>
					<div class="col-2 fl">
						<div class="ewm">
							<em class="line-ico disIb vam">
								<img src="/template/templet1/images/line-ewm.png">
							</em>
							<img src="http://127.0.0.1:81/images/upload/websiteLogo/20161024/1477277314920.jpg" width="130" height="130" class="disIb vam">
						</div>
					</div>
				</section>

			</div>
		</footer>
	</section>
</div>

</section>
</div>

<script src="/template/templet1/js/jquery.js"></script>
<script src="/template/templet1/js/modernizr-custom.js"></script>
<script src="/template/templet1/js/common.js" type="text/javascript"></script>
<script src="/template/templet1/js/swiper-2.1.0.js" type="text/javascript"></script>
<script src="/template/templet1/js/mislider.js"></script>
<script src="/template/templet1/js/footer_common.js" type="text/javascript"></script><!--公共js-->
</body>
</html>

