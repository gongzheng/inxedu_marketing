<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>页面列表</title>
<style type="text/css">
	.pathUrl:hover{
		text-decoration: none;
	}
</style>
<script type="text/javascript">
/**
 * 删除
 */
function delWebpage(id,em){
	if(!confirm('是否删除文件?')){
		return;
	}
	$.ajax({
		url:'/admin/ajax/fileDel/'+id,
		type:'post',
		dataType:'json',
		success:function(result){
			if(result.success==false){
				msgshow(result.message);
			}else{
				$.ajax({
					url:'/admin/ajax/webpageDel/'+id,
					type:'post',
					dataType:'json',
					success:function(result){
						location.reload();
					}
				})
			}
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}
/**
 * 发布
 */
function publishWebpage(id,em){
	if(!confirm('确实要发布吗?')){
		return;
	}
	$.ajax({
		url:'/admin/ajax/webpagePublish/'+id,
		type:'post',
		dataType:'json',
		success:function(result){
			if(result.success==true){
				msgshow("发布成功");
			}else{
				msgshow(result.message);
			}
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}

/*设为首页*/
function setHelloPage(webpageId) {
	$.ajax({
		url:'/admin/ajax/setHelloPage/'+webpageId,
		type:'post',
		dataType:'json',
		success:function(result){
			if(result.success==true){
				msgshow(result.message);
			}else{
				msgshow(result.message);
			}
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}
/*预览*/
function showTemp(webpageId,fileName) {
	var s = fileName;
		$.ajax({
			url:'/admin/ajax/webpagePublish/'+webpageId,
			type:'post',
			dataType:'json',
			success:function(result){
				if(result.success==true){
                    window.open("${ctx}"+s);
				}else{
					msgshow(result.message);
				}
			},
			error:function(error){
				msgshow("系统繁忙，请稍后再操作！");
			}
		});
	}
</script>
</head>
<body>
	<div class="rMain rMain-nb">
		<form action="${ctx}/admin/webpage/list" method="post" id="searchForm">
			<input type="hidden" id="pageCurrentPage" name="page.currentPage" value="1" />
				<input type="text" name="webpage.id" value="${webpage.id }" placeholder="id" />
				<input type="text" name="webpage.pageName" value="${webpage.pageName }" placeholder="页面名称" />
			<a title="查找页面" onclick="$('#searchForm').submit();" class="button tooltip" href="javascript:void(0)">
				<span class="ui-icon ui-icon-search"></span>
				查找页面
			</a>
			<a title="清空" onclick="$('#searchForm input:text').val('');$('#searchForm select').val(0);$('select').change();"
				class="button tooltip" href="javascript:void(0)">
				<span class="ui-icon ui-icon-cancel"></span>
				清空
			</a>
			<a title="一键发布" class="button tooltip" onclick="window.location.href='${ctx}/admin/webpage/publishStatus'" href="javascript:void(0)">一键发布</a>
		</form>
		<table cellspacing="0" cellpadding="0" border="0" class="fullwidth">
			<thead>
				<tr>
					<%--<td align="center">id</td>--%>
					<td align="center">页面名称</td>
					<%--<td align="center">标题</td>
					<td align="center">作者</td>
					<td align="center">关键词</td>
					<td align="center">描述</td>--%>
					<td align="center">模板路径</td>
					<td align="center">操作</td>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${webpageList}" var="webpage"  varStatus="index">
					<tr <c:if test="${index.count%2==1 }">class="odd"</c:if>>
						<%--<td align="center">${webpage.id }</td>--%>
						<td align="center">${webpage.pageName }</td>
						<%--<td align="center">${webpage.title }</td>
						<td align="center">${webpage.author }</td>
						<td align="center">${webpage.keywords }</td>
						<td align="center">${webpage.description }</td>--%>
						<%--<td align="center"><fmt:formatDate value="${webpage.createTime }" pattern="yyyy/MM/dd HH:mm" /></td>--%>
						<td align="center"><a target="_blank" class="pathUrl" href="${ctx}${webpage.publishUrl}">${webpage.publishUrl}</a></td>
						<td align="center">
							<button onclick="delWebpage(${webpage.id},this)" class="ui-state-default ui-corner-all ui-btn" type="button">删除</button>
							<button onclick="window.location.href='${ctx}/admin/webpage/toUpdate/${webpage.id}'" class="ui-btn ui-state-default ui-corner-all" type="button">修改</button>
							<button onclick="publishWebpage(${webpage.id},this)" class="ui-state-default ui-corner-all ui-btn" type="button">发布</button>
							<button onclick="showTemp(${webpage.id},'${webpage.publishUrl}')" class="ui-state-default ui-corner-all ui-btn" type="button">预览</button>
							<button onclick="setHelloPage(${webpage.id})" class="ui-state-default ui-corner-all ui-btn" type="button">设为首页</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<jsp:include page="/WEB-INF/view/common/admin_page.jsp" />
	</div>
</body>
</html>