<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
	<samp class="accordion" id="menu-150">
		<div  style="display: none">
			<h3 class="ui-accordion-header ui-helper-reset ui-state-active ui-corner-top">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<a href="javascript:void(0)" title="系统" class="tooltip"><em class="ico-l-pic ico-l-pic-pz"></em><span class="vam">系统配置</span></a>
			</h3>
			<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: block;">
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/sysuser/userlist" target="mainFrame" data-href="/admin/sysuser/userlist" title="用户列表">用户列表</a>
					</dt>
				</dl>
			</div>
		</div>
		<div  style="display: none">
			<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-e"></span>
				<a href="javascript:void(0)" title="网站信息" class="tooltip"><em class="ico-l-pic ico-l-pic-xx"></em>网站信息</a>
			</h3>
			<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom">
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/websiteProfile/find/web" target="mainFrame" data-href="/admin/websiteProfile/find/web" title="基本配置">基本配置</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/website/navigates" target="mainFrame" data-href="/admin/website/navigates" title="导航管理">导航管理</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/webpage/toadd" target="mainFrame" data-href="/admin/webpage/toadd" title="增加页面">增加页面</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/webpage/list" target="mainFrame" data-href="/admin/webpage/list" title="页面列表" class="current" jerichotabindex="0">页面列表</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/template/file/list" target="mainFrame" data-href="/admin/template/file/list" title="模板管理">模板管理</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/template/file/htmlList" target="mainFrame" data-href="/admin/template/file/htmlList" title="模板列表">模板列表</a>
					</dt>
				</dl>
			</div>
		</div>
		<div id="webUpd">
			<h3 class="ui-accordion-header clearfix">
				<a href="javascript:void (0)" title="页面设置" onclick="templateController()" id="webPage" class="szbtn current"><em class="l-list-ico ymbj">&nbsp;</em><tt class="vam">页面设置</tt></a>
				<a href="javascript:void (0)" title="系统设置" onclick="webController()" id="system" class="szbtn"><em class="l-list-ico xtsz">&nbsp;</em><tt class="vam">系统设置</tt></a>
				<%--<a href="javascript:void(0)" title="模块编辑" class="tooltip"><em class="ico-l-pic ico-l-pic-bj"></em>模块编辑</a>--%>
			</h3>
			<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="webpageTemplateEditor" style="display: block;">
				<c:if test="${webpage.publishUrl=='/index.html'}">
					<div id="navController">
					<dl class="acd-sub-dl">
						<dt>
							<a href="javascript:void (0)" onclick="navigates('INDEX',this)" target="mainFrame"  title="导航管理" class="name"><em class="l-list-ico">&nbsp;</em><tt class="vam">导航管理</tt></a>
						</dt>
					</dl>
					<%--<dl class="acd-sub-dl">
						<dt>
							<a href="javascript:void (0)" onclick="navigates('FRIENDLINK')" target="mainFrame"  title="友情链接" class="name">友情链接</a>
						</dt>
					</dl>--%>
					</div>
				</c:if>
                <form action="${ctx}/admin/webpage/add"  method="post" id="addTemplate">
					<input type="hidden"name="webpage.id" value="${webpage.id}">
					<c:forEach items="${webpageTemplateList}" var="webpageTemplate">
					<dl class="acd-sub-dl">
						<dt>
							<a href="javascript:void (0)" onclick="updSingleTemplate('${webpageTemplate.id}','${webpageTemplate.templateName}')" target="mainFrame" class="name" title="${webpageTemplate.templateName}"><em class="l-list-ico">&nbsp;</em><tt>${webpageTemplate.templateName}</tt></a>
							<a href="javascript:void(0)" onclick="moveUp(this)" title="向上一级" class="acd-ico-btn a-i-b-up"></a>
							<input value="${webpageTemplate.sort}"name="templateSort" class="hiddenSort" type="hidden">
							<input type="hidden" value="${webpageTemplate.id}" name="templateId" class="hiddenDataId">
							<input type="hidden" value="${webpageTemplate.templateUrl}"  name="templateUrl">
							<input type="hidden" value="${webpageTemplate.templateName}"  name="templateName">
							<input type="hidden" value="${webpageTemplate.templateTitle}"  name="templateTitle">
							<input type="hidden" value="${webpageTemplate.info}"  name="info">
							<a href="javascript:void(0)"onclick="moveDown(this)" title="向下一级" class="acd-ico-btn a-i-b-dwon"></a>
							<a href="javascript:void(0)" onclick="$(this).parent().parent().remove();$('iframe').contents().find('#TEMPLATE_LABEL_${webpageTemplate.id}').remove();isDataChange=true;" title="删除" class="acd-ico-btn a-i-b-close"></a>
						</dt>
					</dl>
				</c:forEach>
				</form>
				<div id="webController" style="display: none">

                <dl class="acd-sub-dl" id="webInfo">
                    <dt>
                        <a href="javascript:void (0)" onclick="toWebsiteProfile(this)" target="mainFrame"  title="网站配置"><em class="l-list-ico">&nbsp;</em>网站配置</a>
                    </dt>
                </dl>
					<dl class="acd-sub-dl webController" id="updPwd">
						<dt>
							<a href="javascript:void(0)" id="userInfo" onclick="updPwd(this)" target="mainFrame" title="修改个人信息"><em class="l-list-ico">&nbsp;</em>修改个人信息</a>
						</dt>
					</dl>
				</div>
				<dl class="acd-sub-dl" id="newTemplate">
					<dt>
						<a href="javascript:void(0)" onclick="addNewTemplate(${webpage.id})" target="mainFrame" title="新增模块"><em class="l-list-ico">&nbsp;</em>新增模块<span class="acd-ico-btn a-i-b-zj"></span></a>
					</dt>
				</dl>
				<dl class="acd-sub-dl" style="display: none" id="addNav">
					<dt>
						<a href="javascript:void (0)" onclick="addNavigates()" target="mainFrame" title="新增">新增<span class="acd-ico-btn a-i-b-zj"></span></a>
					</dt>
				</dl>
				<div class="bg-f5 tac pt30 pb50">
					<a href="javascript:void(0)" id="watch" onclick="openPageTemplate()" class="gren-btn btn-big btn mr30">预览</a>
						<a href="javascript:void(0)" onclick="addTemplate(${webpage.id})" id="save" title="生成" class="master-btn btn-big btn">保存</a>
					<c:if test="${webpage.id<=49}">
						<a href="javascript:void(0)" onclick="returnWebpage(${webpage.id})" title="还原" class="gry-btn btn btn-big btn" style="display: none;">还原</a>
					</c:if>

					<a href="javascript:void(0)" id="return" style="display: none" onclick="window.location.href='${ctx}/admin/main'" title="返回" class="master-btn btn-big btn">返回</a>
						<a href="javascript:void(0)" style="display: none" onclick="window.location.href='${ctx}/admin/webpage/publishStatus'" title="一键发布" class="master-btn btn-big btn">一键发布</a>

				</div>
			</div>
		</div>
		<div id="intoTemplateInfo" style="display: none">
			<h3 class="ui-accordion-header clearfix">
				<a href="javascript:void(0)" title="模块编辑" class="tooltip szbtn current">
					<em class="ico-l-pic ico-l-pic-bj">&nbsp;</em>
					<tt class="vam">模块编辑</tt>
				</a>
			</h3>
			<div class="">
				<dl class="acd-sub-dl current">
					<dt>
						<a  href="javascript:void (0)" target="mainFrame"  title=""><em class="l-list-ico">&nbsp;</em><tt  id="templateName"class="vam"></tt></a>
					</dt>
				</dl>
			</div>
			<div class="bg-f5 tac pt30 pb50">
				<a id="updSingleTemplate"  onclick="saveImage()" href="javascript:void (0)" class="master-btn btn-big btn">保存</a>
				<a href="javascript:void(0)" id="normalReturn" onclick="returnPage()" title="返回" class="master-btn btn-big btn">返回</a>
				<a href="javascript:void(0)" onclick="updIndexFrameJump()" style="display: none" id="updIndexFrame" title="返回" class="master-btn btn-big btn">返回</a>
			</div>

		</div>
		<div style="display: none">
			<h3 class="ui-accordion-header clearfix">
				<a href="javascript:void (0)" title="编辑器" class="szbtn current">
					<em class="l-list-ico ymbj">&nbsp;</em><tt class="vam">资讯</tt>
				</a>
			</h3>
			<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom">
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/article/showlist" target="mainFrame" title="资讯列表">资讯列表</a>
					</dt>
				</dl>
				<dl class="acd-sub-dl">
					<dt>
						<a href="/admin/article/initcreate" target="mainFrame" title="添加资讯">添加资讯</a>
					</dt>
				</dl>
			</div>
		</div>
	</samp>

<script>
	//上移操作
	function moveUp(obj){
		var current_id= $(obj).parent().find(".hiddenDataId").val();
		var other_id= $(obj).parent().parent().prev().find(".hiddenDataId").val();

		var current_sort= $(obj).parent().find(".hiddenSort").val();
		var other_sort= $(obj).parent().parent().prev().find(".hiddenSort").val();
		var header = $(obj).parent().parent().prev().prev().find(".hiddenSort").val();

		if(other_sort!=undefined&&header!=undefined){
			$(obj).parent().find(".hiddenSort").val(other_sort);
			$(obj).parent().parent().prev().find(".hiddenSort").val(current_sort);

			$(obj).parent().parent().insertBefore($(obj).parent().parent().prev());    //移动节点

			//页面模板上移操作
			$('iframe').contents().find("#TEMPLATE_LABEL_"+current_id).insertBefore($('iframe').contents().find("#TEMPLATE_LABEL_"+other_id));
			document.getElementById("mainFrame").contentWindow.transparentHeaderTempletMove();//透明头部 第一个模板 下移操作
			//模板 是否 修改 過(排序 ，刪除 ，新增)
			isDataChange=true;
		}
	}
	//下移操作
	function moveDown(obj){
		var current_id= $(obj).parent().find(".hiddenDataId").val();
		var other_id= $(obj).parent().parent().next().find(".hiddenDataId").val();

		var current_sort= $(obj).parent().find(".hiddenSort").val();
		var other_sort= $(obj).parent().parent().next().find(".hiddenSort").val();
		var footer = $(obj).parent().parent().next().next().find(".hiddenSort").val();
		if(other_sort!=undefined&&footer!=undefined){
			$(obj).parent().find(".hiddenSort").val(other_sort);
			$(obj).parent().parent().next().find(".hiddenSort").val(current_sort);

			$(obj).parent().parent().next().insertBefore($(obj).parent().parent());    //移动节点

			//页面模板上移操作
			$('iframe').contents().find("#TEMPLATE_LABEL_"+other_id).insertBefore($('iframe').contents().find("#TEMPLATE_LABEL_"+current_id));
			document.getElementById("mainFrame").contentWindow.transparentHeaderTempletMove();//透明头部 第一个模板 下移操作
			//模板 是否 修改 過(排序 ，刪除 ，新增)
			isDataChange=true;
		}
	}
	$(function () {
		$(".hiddenSort").first().parent().parent().hide();
		$(".hiddenSort").last().parent().parent().hide()
	})
</script>