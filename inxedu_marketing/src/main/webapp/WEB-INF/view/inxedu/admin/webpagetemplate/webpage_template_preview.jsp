<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页-北京因酷时代科技有限公司</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="title" content="">
	<meta name="author" content="北京因酷时代科技有限公司">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/web.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/ie.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/mislider.css">
	<link rel="stylesheet" type="text/css" href="/template/templet1/css/mislider-skin-cameo.css">
	<link href="/template/templet1/css/mw_320_768.css" rel="stylesheet" type="text/css" media="screen and (min-width: 320px) and (max-width: 1024px)">
	<script>
		var str='<link href="/template/templet1/css/color.css?'+Math.random()+'" rel="stylesheet" type="text/css"/>';
		document.write(str);
	</script>
	<!--[if lt IE 9]><script src="/template/templet1/js/html5.js"></script><![endif]-->
	<script src="/template/templet1/js/jquery-1.7.2.min.js" type="text/javascript"></script>
</head>
<body>
<section class="" id="aCoursesList">

${yulan}
</section>
<script src="/template/templet1/js/modernizr-custom.js"></script>
<script src="/template/templet1/js/jquery.js"></script>
<script src="/template/templet1/js/common.js" type="text/javascript"></script>
<script src="/template/templet1/js/mislider.js"></script>
<script src="/template/templet1/js/swiper-2.1.0.js" type="text/javascript"></script>
<script src="/template/templet1/js/footer_common.js" type="text/javascript"></script><!--公共js-->
<script>
    /*预览窗口无法加载xsrc图片预览窗口无法加载xsrc图片兼容*/
	$(function(){
		window.setTimeout("scrollLoad()", 100);
	})

</script>
</body>
</html>

