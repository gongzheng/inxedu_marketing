<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<c:if test="${not empty articleList }">

			<c:forEach var="article" items="${articleList }">
				<li>
					<aside class="n-l-pic">
						<a href="${ctx}/webpage/toArticle/${article.articleId}">
							<img xSrc="${ctx}/${article.imageUrl}" src="/template/templet1/images/default-img.gif">
						</a>
					</aside>
					<h3 class="hLh30 txtOf unFw">
						<a href="${ctx}/webpage/toArticle/${article.articleId}" class="n-l-tit fsize18 c-333 f-fM" title="${article.title}">
							${article.title}
						</a>
					</h3>

					<section class="txt-nr mt10">
						<p class="c-999 f-fM">${article.summary}</p>
					</section>
					<section class="hLh30 clearfix">
						<div class="fl">
							<em class="icon16 ico">
								<img src="/template/templet1/images/liul.png">
							</em>
							<tt class="fsize12 c-666 f-fM vam">${article.clickNum}次</tt>
						</div>
						<div class="fl ml30">
							<em class="icon16 ico">
								<img src="/template/templet1/images/time.png">
							</em>
							<tt class="fsize12 c-666 f-fM vam"><fmt:formatDate value="${article.publishTime }" pattern="yyyy-MM-dd HH:mm" /></tt>
						</div>
					</section>

				</li>
			</c:forEach>
		</ul>
	</article>
	<article class="container">
		<!-- 公共分页 开始 -->
		<form action="${ctx }/front/ajax/articlelist" method="post" id="searchForm">
			<input type="hidden" name="page.currentPage" id="pageCurrentPage" value="1">
		</form>
		<div>
			<jsp:include page="/WEB-INF/view/common/ajaxpage-multi.jsp"></jsp:include>
			<div class="clear"></div>
		</div>
		<!-- 公共分页 结束 -->
	</article>

</c:if>