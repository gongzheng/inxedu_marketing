<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,user-scalable=no,minimal-ui" name="viewport">
<title><sitemesh:write property='title'/></title>
<link type="text/css" href="${ctx}/static/admin/css/layout.css" rel="stylesheet" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<meta content="INXEDU(http://www.inxedu.com)" name="author">
<meta content="INXEDU,在线教育,网校搭建,网校,网络教育,远程教育,云网校,在线学习,在线考试" name="keywords">
<meta content="inxedu是一家专注“在线教育平台”的互联网公司，在国内属顶级在线教育解决方案提供商中的领跑者。为大、中型客户提供领先的在线教育平台方案服务。" name="description">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black-translucent" name="apple-mobile-web-app-status-bar-style">
<meta name="format-detection" content="telephone=no">
<script type="text/javascript" src="${ctx}/static/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/admin/js/admin-inxedu.js"></script>
<script type="text/javascript" src="${ctximg}/static/common/webutils.js"></script>
<script type="text/javascript">
    baselocation='${ctx}';
    keuploadSimpleUrl='<%=keuploadSimpleUrl%>';
    uploadSimpleUrl='<%=uploadSimpleUrl%>';
    imagesPath='<%=staticImage%>';
    </script>
	<sitemesh:write property='head'/>
</head>
<body>
	<sitemesh:write property='body'/>
	<%--<script>
		$(function(){
			$("section").hide();
		});
	</script>--%>
</body>
</html>
